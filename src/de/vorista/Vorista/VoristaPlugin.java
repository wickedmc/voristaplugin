package de.vorista.Vorista;

import de.vorista.Vorista.commands.*;
import de.vorista.Vorista.listeners.*;
import de.vorista.Vorista.utils.*;
import de.vorista.Vorista.votecrates.VoteCrate;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Corvin on 03.08.2016.
 */
public class VoristaPlugin extends JavaPlugin {

    private static VoristaPlugin instance;
    private static Messages messages;
    private static CustomCmdManager customCommand;
    private static boolean debug = false;
    private AutoMessenger autoMessenger;

    /*

    |-----------------------------------------------------------------------------|
    |                               TODO                                          |
    |-----------------------------------------------------------------------------|

        - Wetter und Tag vote
        - 30 lvl bei 30er enchant verlieren

     */

    @Override
    public void onEnable() {
        instance = this;
        registerExternalPlugins();

        VoteCrate.load();
        Tag.loadTags();

        messages = new Messages(this);
        autoMessenger = new AutoMessenger(this);
        autoMessenger.start();

        customCommand = new CustomCmdManager();
        registerCommandsAndListeners();

        sendToConsole("&bVorista Plugin &aloaded up!");
        saveDefaultConfig();
    }

    @Override
    public void onDisable() {
        VoteCrate.stopTask();
        VoteCrate.clearHolograms();
        Tag.saveTags();
        instance = null;
        sendToConsole("&bVorista Plugin &cshut down!");
    }

    private void registerCommandsAndListeners() {
        PluginManager pm = Bukkit.getServer().getPluginManager();
        CmdHelpChat cmdHelpChat = new CmdHelpChat();
        pm.registerEvents(new ChatListener(), this);
        pm.registerEvents(cmdHelpChat, this);
        pm.registerEvents(new PlayerListener(), this);
        pm.registerEvents(new BlockListener(), this);
        pm.registerEvents(new VoteListener(), this);
        pm.registerEvents(new TagMenu(), this);
        pm.registerEvents(new EntityKillListener(), this);
        //pm.registerEvents(new ShopBuyListener(), this);

        getCommand("ticket").setExecutor(cmdHelpChat);
        getCommand("vorista").setExecutor(new CmdVorista());
        getCommand("customcmd").setExecutor(new CmdCustomCmd());
        getCommand("votecrate").setExecutor(new CmdVotecrate());
        getCommand("itembuilder").setExecutor(new CmdItembuilder());
        getCommand("tag").setExecutor(new CmdTag());
        //getCommand("test").setExecutor(new CmdTest());
        getCommand("vote").setExecutor(new CmdVote());
        getCommand("giveall").setExecutor(new CmdGiveall());
        getCommand("adminalert").setExecutor(new CmdAdminalert());
        getCommand("automessenger").setExecutor(new CmdAutomessenger());
    }

    private void registerExternalPlugins() {

    }

    public AutoMessenger getAutoMessenger() { return autoMessenger; }
    public static VoristaPlugin getInstance() { return instance; }
    public static Messages getMessages() { return messages; }
    public static CustomCmdManager getCustomCommand() { return customCommand; }
    public static void sendToConsole(String msg) { Bukkit.getConsoleSender().sendMessage(Utils.color(msg)); }
    public static void debug(String msg) { if(debug) sendToConsole("&4DEBUG: &c"+msg); }

}
