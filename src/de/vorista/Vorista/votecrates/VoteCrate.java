package de.vorista.Vorista.votecrates;


import darkblade12.utils.ParticleEffect;
import de.vorista.Vorista.VoristaPlugin;
import de.vorista.Vorista.utils.ItemStackBuilder;
import de.vorista.Vorista.utils.Utils;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * Created by Corvin on 02.10.2016.
 */
public class VoteCrate {

    private static Map<Block, ArmorStand> crates = new HashMap<>();
    private static BukkitRunnable runnable;

    private static File file;
    private static FileConfiguration config;

    private static ItemStack KEY;
    private static ItemStack CRATE_ITEM;
    private static List<VoteReward> rewards = new ArrayList<>();

    // Settings
    private static boolean ANIMATE_TEXT = true;
    private static boolean USE_PARTICLES = true;
    private static Effect PARTICLE_EFFECT = Effect.WATERDRIP;

    private static double COUNTER;

    public static void load() {
        Material keyMat = Material.TRIPWIRE_HOOK;
        Material itemMat = Material.ENDER_CHEST;

        file = new File(VoristaPlugin.getInstance().getDataFolder(), "votecrates.yml");
        if(!file.exists()) {
            try {
                file.createNewFile();
                FileWriter writer = new FileWriter(file);
                writer.write("settings:\n");
                writer.write("  materials:\n");
                writer.write("    key: TRIPWIRE_HOOK\n");
                writer.write("    crate: ENDER_CHEST");
                writer.write("  cosmetics:\n");
                writer.write("    animateText: "+ANIMATE_TEXT+"\n");
                writer.write("    useParticles: "+USE_PARTICLES);
                writer.write("    effect: "+PARTICLE_EFFECT.name()+"\n");
                writer.flush();
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        config = YamlConfiguration.loadConfiguration(file);

        try {
            ANIMATE_TEXT = config.getBoolean("settings.cosmetics.animateText");
            USE_PARTICLES = config.getBoolean("settings.cosmetics.useParticles");

            PARTICLE_EFFECT = Effect.valueOf(config.getString("settings.cosmetics.particleEffect"));

            keyMat = Material.valueOf(config.getString("settings.materials.key"));
            itemMat = Material.valueOf(config.getString("settings.materials.crate"));
        } catch(Exception ex) { ex.printStackTrace(); }

        KEY = new ItemStackBuilder(keyMat)
                .withName("&9&lVOTE &f&lKEY")
                .withLore("&7Benutz diesen Schlüssel")
                .withLore("&7auf eine Votecrate!")
                .withEnchantment(Enchantment.ARROW_INFINITE)
                .withItemFlag(ItemFlag.HIDE_ENCHANTS)
                .build();
        CRATE_ITEM = new ItemStackBuilder(itemMat)
                .withName("&9Votecrate Setzen")
                .withLore("&7Setze eine Votecrate")
                .build();

        loadCrates();
        loadRewards();
        startTask();
    }

    public static boolean isCrate(Block block) { return crates.keySet().contains(block); }
    public static boolean isKey(ItemStack itemStack) { return Utils.compareItems(itemStack, KEY, true); }
    public static boolean isCrateItem(ItemStack itemStack) {  return Utils.compareItems(itemStack, CRATE_ITEM, false); }
    public static boolean isAnimateText() { return ANIMATE_TEXT; }
    public static boolean isUseParticles() { return USE_PARTICLES; }
    public static Effect getParticleEffect() { return PARTICLE_EFFECT; }
    public static double getCounter() { return COUNTER; }
    public static void setAnimateText(boolean b) { ANIMATE_TEXT = b; config.set("settings.cosmetics.animateText", b); saveFile(); }
    public static void setUseParticles(boolean b) { USE_PARTICLES = b; config.set("settings.cosmetics.useParticles", b); saveFile(); }
    public static void setParticleEffect(Effect effect) { PARTICLE_EFFECT = effect; config.set("settings.cosmetics.particleeffect", effect.getName()); saveFile();}
    public static ArmorStand getArmorStandOf(Block block) { return crates.get(block); }
    public static Map<Block, ArmorStand> getCrates() { return crates; }
    public static List<VoteReward> getRewards() { return rewards; }
    public static ItemStack getKey() { return KEY; }
    public static ItemStack getCrateItem() { return CRATE_ITEM; }
    public static FileConfiguration getConfig() { return config; }

    //  |--------------------------------------------------------------------------------------------|
    //  |       C   R   A   T   E   S                                                                |
    //  |--------------------------------------------------------------------------------------------|

    public static boolean addCrate(Block block, boolean save) {
        if(!isCrate(block)) {
            ArmorStand armorStand = (ArmorStand) block.getWorld().spawnEntity(block.getLocation().add(0.5,0.5,0.5), EntityType.ARMOR_STAND);
            armorStand.setGravity(false);
            armorStand.setSmall(true);
            armorStand.setBasePlate(false);
            armorStand.setVisible(false);
            armorStand.setCustomName(Utils.color("&7&l=> &9&lVOTE CRATE &7&l<="));
            armorStand.setCustomNameVisible(true);
            crates.put(block, armorStand);

            if(save) {
                saveCrateToFile(block);
            }
        }
        return isCrate(block);
    }

    public static boolean removeCrate(Block block) {
        if(isCrate(block)) {
            crates.get(block).remove();
            crates.remove(block);

            List<String> crateStrings = config.getStringList("crates");
            crateStrings.remove(crateToString(block));
            config.set("crates", crateStrings);
        }
        return isCrate(block);
    }

    public static void saveCrateToFile(Block block) {
        List<String> crateStrings = config.getStringList("crates");
        if(!crateStrings.contains(crateToString(block))) crateStrings.add(crateToString(block));
        config.set("crates", crateStrings);
        saveFile();
    }

    public static void loadCrates() {
        List<String> crateStrings = config.getStringList("crates");
        for(String s : crateStrings) {
            addCrate(stringToCrate(s), false);
        }
    }

    public static void clearHolograms() {
        for(ArmorStand as : crates.values()) as.remove();
    }

    //  |--------------------------------------------------------------------------------------------|
    //  |       R   E   W   A   R   D   S                                                            |
    //  |--------------------------------------------------------------------------------------------|

    public static void loadRewards() {
        for(VoteReward reward : getRewardsFromFile()) {
            rewards.add(reward);
        }
    }

    public static void addReward(VoteReward reward) {
        rewards.add(reward);

        List<String> list = config.getStringList("rewards");
        list.add(reward.toString());
        config.set("rewards", list);
        saveFile();
    }
    public static void setReward(VoteReward reward, int index) {
        rewards.set(index, reward);
        List<String> list = config.getStringList("rewards");
        list.set(index, reward.toString());
        config.set("rewards", list);
        saveFile();
    }
    public static void removeReward(int index) {
        rewards.remove(index);
        List<String> list = config.getStringList("rewards");
        list.remove(index);
        config.set("rewards", list);
        saveFile();
    }

    public static List<VoteReward> getRewardsFromFile() {
        List<VoteReward> rewardList = new ArrayList<>();
        for(String reward : config.getStringList("rewards")) {
            double chance = 0d;
            String name = "";
            String command = "";
            for(String s : reward.split(" ")) {
                if (s.startsWith("chance:")) chance = Double.parseDouble(s.replaceFirst("chance:", ""));
                if (s.startsWith("rewardname:")) name = s.replaceFirst("rewardname:", "").replaceAll("_", " ");
                if (s.startsWith("command:")) command = s.replaceFirst("command:", "").replaceAll("_", " ");
            }
            if(command.isEmpty()) {
                rewardList.add(new VoteReward(name, Utils.itemStackFromString(reward), null, chance));
            } else {
                rewardList.add(new VoteReward(name, null, command, chance));
            }
        }
        return rewardList;
    }

    public static VoteReward getRandomReward() {
        // Compute the total weight of all items together
        double totalWeight = 0.0d;
        for (VoteReward i : rewards)
        {
            totalWeight += i.getChance();
        }
        // Now choose a random item
        int randomIndex = -1;
        double random = Math.random() * totalWeight;
        for (int i = 0; i < rewards.size(); ++i)
        {
            random -= rewards.get(i).getChance();
            if (random <= 0.0d)
            {
                randomIndex = i;
                break;
            }
        }
        return rewards.get(randomIndex);
    }

    public static void startTask() {
        if(runnable == null) {
            runnable = new BukkitRunnable() {

                int period = 0;
                double counter = 0;
                Map<Block, Integer> characters = new HashMap<>();
                List<String> names = Arrays.asList(
                        "&7&l=> &9&lV&f&lOTE CRATE &7&l<=",
                        "&7&l=> &f&lV&9&lO&f&lTE CRATE &7&l<=",
                        "&7&l=> &f&lVO&9&lT&f&lE CRATE &7&l<=",
                        "&7&l=> &f&lVOT&9&lE &f&lCRATE &7&l<=",
                        "&7&l=> &f&lVOTE&9&l C&f&lRATE &7&l<=",
                        "&7&l=> &f&lVOTE C&9&lR&f&lATE &7&l<=",
                        "&7&l=> &f&lVOTE CR&9&lA&f&lTE &7&l<=",
                        "&7&l=> &f&lVOTE CRA&9&lT&f&lE &7&l<=",
                        "&7&l=> &f&lVOTE CRAT&9&lE &7&l<=",
                        "&7&l=> &f&lVOTE CRATE &7&l<=", // 9
                        "&8&l=> &9&lVOTE CRATE &8&l<=", // 10
                        "&7&l=> &f&lVOTE CRATE &7&l<=", // 11
                        "&8&l=> &9&lVOTE CRATE &8&l<=", // 12
                        "&7&l=> &f&lVOTE CRATE &7&l<="); // 13

                @Override
                public void run() {

                    for(Block block : crates.keySet()) {
                        characters.putIfAbsent(block, 0);
                        int character = characters.get(block);
                        Location location = block.getLocation();

                        if(USE_PARTICLES) {
                            for (double y = location.getY() - 1; y < location.getY() + 2.25; y += 0.1) {
                                double x = location.getX() + 0.5 + (2 * Math.sin(20 * y - counter));
                                double z = location.getZ() + 0.5 + (2 * Math.cos(20 * y  - counter));

                                Location newLoc = new Location(location.getWorld(), x, y, z);
                                ParticleEffect effect = ParticleEffect.WATER_WAKE;
                                effect.display(0,0,0,0,10,newLoc, 100);
                            }

                            if(characters.get(block) == 9) {
                                // Radius und Kreise machen!!
                                for (double y = location.getY() + 1; y > location.getY()+0.25; y -= 0.25) {
                                    for(int d = 0; d <= 360; d += 10) {
                                        double x2 = location.getX() + 0.5 + (Math.cos(d));
                                        double z2 = location.getZ() + 0.5 + (Math.sin(d));

                                        double x3 = location.getX() + 0.5 + (0.25*Math.cos(d));
                                        double z3 = location.getZ() + 0.5 + (0.25*Math.sin(d));

                                        Location newLoc2 = new Location(location.getWorld(), x2, y+1, z2);
                                        ParticleEffect effect = ParticleEffect.FIREWORKS_SPARK;
                                        effect.display(0,0,0,0,1,newLoc2, 100);
                                    }
                                }
                            }
                        }

                        if(ANIMATE_TEXT) {
                            crates.get(block).setCustomName(Utils.color(names.get(character)));
                            character++;
                            if (character == names.size()) character = 0;
                            characters.put(block, character);
                        }

                    }
                    period++;

                    counter += 0.1;

                    COUNTER = counter;
                    if(period >= 10) period = 0;
                }
            };
            runnable.runTaskTimer(VoristaPlugin.getInstance(), 0, 4);
        }
    }

    public static void stopTask() {
        if(runnable != null) {
            runnable.cancel();
            runnable = null;
        }
    }

    private static String crateToString(Block block) { return block.getWorld().getName()+" "+block.getX()+"/"+block.getY()+"/"+block.getZ(); }

    private static Block stringToCrate(String s) {
        String[] coords = s.split(" ")[1].split("/");
        int x,y,z;
        try {
            x = Integer.parseInt(coords[0]);
            y = Integer.parseInt(coords[1]);
            z = Integer.parseInt(coords[2]);
        } catch(NumberFormatException ex) {
            return null;
        }
        return VoristaPlugin.getInstance().getServer().getWorld(s.split(" ")[0]).getBlockAt(x, y, z);
    }


    public static void reload() {
        clearHolograms();
        crates.clear();
        rewards.clear();
        stopTask();

        load();
    }

    private static void saveFile() {
        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
