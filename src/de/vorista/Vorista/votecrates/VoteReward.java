package de.vorista.Vorista.votecrates;

import de.vorista.Vorista.VoristaPlugin;
import de.vorista.Vorista.utils.Utils;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Corvin on 08.10.2016.
 */
public class VoteReward {

    private String name;
    private String command;
    private ItemStack itemStack;
    private double percentage;

    public VoteReward(String name, ItemStack itemStack,  String command, double percentage)  {
        this.command = command;
        this.itemStack = itemStack;

        this.name = name;
        this.percentage = percentage;
    }

    public boolean isCommand() { return command != null; }
    public String getName() { return name; }
    public double getChance() { return percentage; }
    public ItemStack getItem() { return itemStack; }
    public String getCommand() { return command; }

    public String toString() {
        String s;
        VoristaPlugin.debug(isCommand()+"");
        VoristaPlugin.debug(name);
        VoristaPlugin.debug(percentage+"");
        VoristaPlugin.debug(command);
        //VoristaPlugin.debug(itemStack.toString());

        if(isCommand()) {
            s = "rewardname:"+name.replaceAll(" ", "_")+" chance:"+percentage+" "+"command:"+command.replaceAll(" ", "_");
        } else {
            s = "rewardname:"+name.replaceAll(" ", "_")+" chance:"+percentage+" "+ Utils.itemStackToString(itemStack);
        }
        return s;
    }

}
