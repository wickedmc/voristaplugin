package de.vorista.Vorista;

import de.vorista.Vorista.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Corvin on 22.10.2016.
 */
public class AutoMessenger extends BukkitRunnable {

    private final VoristaPlugin plugin;

    private List<String> messages;
    private int minuteInterval;
    private  String prefix;
    private int currentIndex;
    private int counter = 0;

    private File messageFile;
    private FileConfiguration config;

    public AutoMessenger(VoristaPlugin plugin) {
        this.plugin = plugin;
        loadFile();
        loadMessages();
    }

    @Override
    public void run() {
        counter++;

        if(counter >= minuteInterval) {
            counter = 0;
            String message = messages.get(currentIndex);

            Bukkit.broadcastMessage("");
            Bukkit.broadcastMessage(Utils.color(prefix+message));
            Bukkit.broadcastMessage("");

            currentIndex++;
            if(currentIndex >= messages.size()) currentIndex = 0;
        }
    }

    private void loadFile() {
        messageFile = new File(plugin.getDataFolder(), "automessages.yml");

        if(!messageFile.exists()) {
            try {
                messageFile.createNewFile();
                FileWriter fw = new FileWriter(messageFile);
                fw.write("prefix: '&e[AutoMessenger] &f'\n");
                fw.write("interval: 10");
                fw.flush();
                fw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        config = YamlConfiguration.loadConfiguration(messageFile);
    }

    public void loadMessages() {
        messages = new ArrayList<>();
        if(config.contains("messages"))
            messages.addAll(config.getStringList("messages").stream().collect(Collectors.toList()));
        if(config.contains("prefix"))
            prefix = config.getString("prefix");
        minuteInterval = config.getInt("interval");
    }

    public void reload() {
        loadFile();
        loadMessages();
    }

    public void start() {
        this.runTaskTimer(VoristaPlugin.getInstance(), 0, 20*60);
    }

    public void stop() {
        this.cancel();
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
        config.set("prefix", prefix);
        saveFile();
    }

    public void addMessage(String message) {
        messages.add(message);
        List<String> configMessages = getConfigMessages();
        configMessages.add(message);
        config.set("messages", configMessages);
        saveFile();
    }

    public void removeMessage(int index) {
        messages.remove(index);
        List<String> configMessages = getConfigMessages();
        configMessages.remove(index);
        config.set("messages", configMessages);
        saveFile();
    }

    public void setMinuteInterval(int minutes) {
        minuteInterval = minutes;
        config.set("interval", minutes);
        saveFile();
    }

    public List<String> getMessages() { return messages; }
    public int getMinuteInterval() { return minuteInterval; }
    public String getPrefix() { return prefix; }

    private List<String> getConfigMessages() { return config.getStringList("messages"); }

    private void saveFile() {
        try {
            config.save(messageFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
