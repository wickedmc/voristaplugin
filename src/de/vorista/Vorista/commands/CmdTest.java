package de.vorista.Vorista.commands;

import de.vorista.Vorista.utils.ItemStackBuilder;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.SpawnEgg;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Corvin on 08.10.2016.
 */
public class CmdTest implements CommandExecutor {

    private List<Player> playerList = new ArrayList<>();
    private BukkitRunnable runnable;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(!(sender instanceof Player)) return true;

        Player player = (Player) sender;

        ItemStack itemStack = new ItemStackBuilder(Material.MONSTER_EGG).withName("Data2").withData(EntityType.PIG.ordinal()).build();
        ItemStack itemStack2 = new ItemStackBuilder(Material.MONSTER_EGG).withName("Dura").withDurability(120).build();
        ItemStack itemStack3 = (new SpawnEgg(EntityType.PIG)).toItemStack();

        player.getInventory().addItem(itemStack);
        player.getInventory().addItem(itemStack2);
        player.getInventory().addItem(itemStack3);

        return true;
    }
}
