package de.vorista.Vorista.commands;

import de.vorista.Vorista.VoristaPlugin;
import de.vorista.Vorista.utils.Utils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Corvin on 08.10.2016.
 */
public class CmdVote implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(!(sender instanceof Player)) return true;

        Player player = (Player)sender;

        player.sendMessage(Utils.color("&9--------------- &7Vote hier &9---------------"));
        for(String s : VoristaPlugin.getInstance().getConfig().getStringList("voteLinks"))
            player.sendMessage(Utils.color("&7- &e"+s));
        player.sendMessage(Utils.color("&9---------------------------------------"));

        return true;
    }
}
