package de.vorista.Vorista.commands;

import de.vorista.Vorista.VoristaPlugin;
import de.vorista.Vorista.utils.ItemStackBuilder;
import de.vorista.Vorista.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Corvin on 16.10.2016.
 */
public class CmdGiveall implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(!(sender instanceof Player)) return true;

        Player player = (Player) sender;

        if(!player.hasPermission("vorista.giveall")) {
            player.sendMessage(Utils.color("&7[&9V&7] Keine Rechte."));
            return true;
        }

        ItemStack itemStack = player.getItemInHand();

        if(itemStack == null || itemStack.getType() == Material.AIR) {
            player.sendMessage(Utils.color("&7[&9V&7] Bitte nimm ein Item in die Hand"));
            return true;
        }

        int amount = itemStack.getAmount();
        if(args.length >= 1) {
            try {
                amount = Integer.parseInt(args[0]);
            } catch(NumberFormatException ex) {  }
        }

        String name = itemStack.getType().name();
        if(itemStack.hasItemMeta() && itemStack.getItemMeta().hasDisplayName()) {
            name = itemStack.getItemMeta().getDisplayName();
        }

        ItemStack itemToGive = new ItemStackBuilder(itemStack).withAmount(amount).build();
        String itemString = amount+"x "+name;

        for(Player p : Bukkit.getOnlinePlayers()) {
            p.getInventory().addItem(itemToGive);
            p.sendMessage(Utils.color("&7[&9V&7] Du hast &9"+itemString+" &7bekommen!"));
        }

        VoristaPlugin.sendToConsole(player.getName()+" benutzte /giveall: "+itemString);
        player.sendMessage(Utils.color("&7[&9V&7] Allen Spielern &9"+itemString+" &7gegeben!"));

        return true;
    }

}
