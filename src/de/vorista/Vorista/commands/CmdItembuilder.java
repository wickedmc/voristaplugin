package de.vorista.Vorista.commands;

import de.vorista.Vorista.VoristaPlugin;
import de.vorista.Vorista.utils.CustomCmdManager;
import de.vorista.Vorista.utils.ItemStackBuilder;
import de.vorista.Vorista.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Corvin on 02.10.2016.
 */
public class CmdItembuilder implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (!(sender instanceof Player)) return true;

        Player player = (Player) sender;

        if (!player.hasPermission("vorsita.itembuilder")) {
            player.sendMessage(Utils.color("&7[&9V&7] Keine Permission"));
            return true;
        }

        CustomCmdManager manager = VoristaPlugin.getCustomCommand();
        ItemStack itemInHand = player.getItemInHand();
        ItemStackBuilder itemBuilder = new ItemStackBuilder(itemInHand);

        if (itemInHand == null || itemInHand.getType() == Material.AIR) {
            player.sendMessage(Utils.color("&7[&9V&7] Bitte nimm ein Item in die Hand."));
            return true;
        }

        if (args.length == 0) {
            Bukkit.dispatchCommand(player, "ib help");
            return true;
        } else {
            String subcommand = args[0];

            if (subcommand.equalsIgnoreCase("name")) {
                // /ib name <name>
                if (args.length < 2) {
                    Bukkit.dispatchCommand(player, "ib help");
                    return true;
                }
                String name = "";
                for (int i = 1; i < args.length; i++) name += " " + args[i];
                name = name.replaceFirst(" ", "");
                itemBuilder.withName(name);
                player.setItemInHand(itemBuilder.build());
                player.updateInventory();
                player.sendMessage(Utils.color("&7[&9V&7] Name des Items geändert zu '&r" + name + "&7'"));
                return true;
            } else if (subcommand.equalsIgnoreCase("addlore")) {
                // /ib addlore <line>
                if (args.length < 2) {
                    Bukkit.dispatchCommand(player, "ib help");
                    return true;
                }
                String line = "";
                for (int i = 1; i < args.length; i++) line += " " + args[i];
                line = line.replaceFirst(" ", "");

                itemBuilder.withLore(line);
                player.setItemInHand(itemBuilder.build());
                player.updateInventory();
                player.sendMessage(Utils.color("&7[&9V&7] Lore-Zeile hinzugefügt."));
                return true;
            } else if (subcommand.equalsIgnoreCase("removelore")) {
                // /ib removelore <index>
                if (args.length != 2) {
                    Bukkit.dispatchCommand(player, "ib help");
                    return true;
                }
                int index = -1;
                try {
                    index = Integer.parseInt(args[1]);
                } catch (NumberFormatException ex) {
                    Bukkit.dispatchCommand(player, "ib help");
                    return true;
                }
                itemBuilder.removeLore(index);
                player.setItemInHand(itemBuilder.build());
                player.updateInventory();
                player.sendMessage(Utils.color("&7[&9V&7] Lore-Zeile wurde gelöscht."));
                return true;
            } else if (subcommand.equalsIgnoreCase("listlore")) {
                // /ib listlore
                if (args.length != 1) {
                    Bukkit.dispatchCommand(player, "ib help");
                    return true;
                }
                List<String> lore = itemBuilder.getLore();
                int items = -1;
                player.sendMessage(Utils.color("&9---------------------------"));
                for (String line : lore) {
                    items++;
                    player.sendMessage(Utils.color("&7[&9" + items + "&7] &5&o" + line));
                }
                if (items == -1) player.sendMessage(Utils.color("&7&o- leer -"));
                player.sendMessage(Utils.color("&9---------------------------"));
                return true;
            } else if (subcommand.equalsIgnoreCase("amount")) {
                // /ib amount <amount>
                if (args.length != 2) {
                    Bukkit.dispatchCommand(player, "ib help");
                    return true;
                }
                int amount = -1;
                try {
                    amount = Integer.parseInt(args[1]);
                } catch (NumberFormatException ex) {
                    Bukkit.dispatchCommand(player, "ib help");
                    return true;
                }

                itemBuilder.withAmount(amount);
                player.setItemInHand(itemBuilder.build());
                //player.updateInventory();
                player.sendMessage(Utils.color("&7[&9V&7] Amount gesetzt auf &9" + amount));
                return true;
            } else if (subcommand.equalsIgnoreCase("enchant")) {
                // /ib enchant <enchantment> <level>
                if (args.length != 3) {
                    Bukkit.dispatchCommand(player, "ib help");
                    return true;
                }

                String enchantString = args[1];
                int level = -1;
                try {
                    level = Integer.parseInt(args[2]);
                } catch (NumberFormatException ex) {
                    Bukkit.dispatchCommand(player, "ib help");
                    return true;
                }
                Enchantment enchantment = Enchantment.getByName(enchantString.toUpperCase());
                if (enchantment == null) {
                    player.sendMessage(Utils.color("&7[&9V&7] Ungültiges Enchantment. Gültige Enchantments:"));
                    List<String> enchantments = new ArrayList<>();
                    for (Enchantment e : Enchantment.values()) {
                        enchantments.add(e.getName());
                    }
                    player.sendMessage(Utils.commaList(enchantments));
                    return true;
                }
                itemBuilder.withEnchantment(enchantment, level);
                player.setItemInHand(itemBuilder.build());
                player.updateInventory();
                player.sendMessage(Utils.color("&7[&9V&7] Item enchanted."));
                return true;
            } else if (subcommand.equalsIgnoreCase("help")) {
                List<String> helpLines = Arrays.asList(
                        "&7-----<= &9Hilfe für /itembuilder oder /ib &7=>-----",
                        "&9/ib name <name> &7- Gib deinem Item einen Namen",
                        "&9/ib addlore <line> &7- Füge der Lore eine Zeile hinzu",
                        "&9/ib removelore <index> &7- Lösche die Zeile <index> von der Lore",
                        "&9/ib listlorex> &7- Listet die Lore auf",
                        "&9/ib amount <amount> &7- Setze den Amount deines Items",
                        "&9/ib enchant <enchantment> <level> &7- Enchante dein Item",
                        "&9/ib help &7- Zeigt diese Hilfe an");

                for (String s : helpLines) player.sendMessage(Utils.color(s));
                return true;
            }

            return true;
        }
    }
}
