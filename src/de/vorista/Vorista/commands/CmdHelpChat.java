package de.vorista.Vorista.commands;

import de.vorista.Vorista.VoristaPlugin;
import de.vorista.Vorista.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * Created by Corvin on 07.10.2016.
 */
public class CmdHelpChat implements CommandExecutor, Listener {

    //	private File file = new File("plugins/WickedCore", "suppchats.yml");
//	private YamlConfiguration cfg = YamlConfiguration.loadConfiguration(file);

    private Set<Player> playerQueue = new HashSet<>();
    private Set<Player> spies = new HashSet<>();
    private static HashMap<Player, Player> supportChats = new HashMap<>(); // Supporter, User


    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String lbl,
                             String[] args) {

		/*	Spieler -> /support [Grund]
		 * 	Nachricht an Team
		 *  Supporter -> /support accept <Spieler>
		 *  Chat Chat Chat
		 *  Spieler/Supporter -> /support close
		 *
		 *  Zus�tzlich: /support spy
		 *  			/support list
		 *  			/support deny <Spieler>
		 *  			/support help
		 */

        if(!(sender instanceof Player)) {
            return true;
        }

        Player player = (Player) sender;

        if(!player.hasPermission("vorista.ticket.staff")) {

            if(args.length >= 1 && args[0].equalsIgnoreCase("close")) {
                if(!(supportChats.containsValue(player))) {
                    player.sendMessage(Utils.color("&7Du bist in keinem Chat!"));
                    return true;
                }

                for(Player key : supportChats.keySet()) {
                    if(supportChats.get(key) == player) {
                        key.sendMessage(Utils.color("&9"+player.getName()+" &7hat den Chat geschlossen!"));
                        player.sendMessage(Utils.color("&7Du hast den Chat geschlossen!"));
                        supportChats.remove(key);
                        return true;
                    }
                }
                player.sendMessage(Utils.color("&cDa ging etwas schief, das tut uns Leid!"));
                return true;
            }

            // Spieler -> Anfrage �ffnen
            if(playerQueue.contains(player) || supportChats.containsValue(player)) {
                player.sendMessage(Utils.color("&7Du bist bereits in der Support-Warteschlange!"));
                return true;
            }
            String grund = "";
            if(args.length > 0) {
                if(args[0].equalsIgnoreCase("help")) {
                    Bukkit.dispatchCommand(player, "ticket help");
                    return true;
                } else {
                    for(int i = 0; i < args.length; i++) {
                        grund += args[i]+" ";
                    }
                }
            } else {
                grund = "kein Grund angegeben";
            }

            playerQueue.add(player);
            sendTeam("&9&lTICKET &8\u2503 &9"+player.getName()+" &7braucht Hilfe! [&9"+grund+"&7]");
            player.sendMessage(Utils.color("&7Du wurdest in die Warteschlange eingetragen. Bitte gedulde dich bis ein Teammitglied frei ist."));
            return true;
        } else {

            if(args.length == 0) {
                Bukkit.dispatchCommand(player, "ticket help");
                return true;
            } else if(args.length == 1) {
                switch(args[0].toLowerCase()) {
                    case "accept": player.sendMessage(Utils.color("&7/ticket accept <Spieler>")); return true;
                    case "deny": player.sendMessage(Utils.color("&7/ticket deny <Spieler>")); return true;

                    case "list":
                        String list = "";
                        if(playerQueue.isEmpty()) {
                            list = "&c-leer-";
                        } else {
                            for(Player p : playerQueue) {
                                list+="&7, &9"+p.getName();
                            }
                            list = list.replaceFirst("&7, ", "");
                        }
                        player.sendMessage(Utils.color("&7-----<= &9Aktuelle Warteschlange: &7=>-----"));
                        player.sendMessage(Utils.color(list));
                        return true;
                    case "spy":
                        if(!player.hasPermission("vorista.ticket.spy")) {
                            player.sendMessage(Utils.color("&7Du darfst das nicht!"));
                            return true;
                        } else {
                            boolean spy = spies.contains(player);
                            if(!spy) spies.add(player);
                            player.sendMessage(Utils.color(!spy ?
                                    "&7Du liest nun alle Ticket-Chats" : "&7Du liest nicht mehr alle Ticket-Chats"));
                            return true;
                        }
                    case "close":
                        if(!supportChats.containsKey(player)) {
                            player.sendMessage(Utils.color("&7Du bist nicht im Ticket-Chat!"));
                            return true;
                        } else {
                            sendTeam("&9"+player.getName()+" &7hat den Chat mit &9"+supportChats.get(player).getName()+" &7geschlossen!");
                            supportChats.get(player).sendMessage(Utils.color("&9"+player.getName()+" &7hat den Chat geschlossen!"));
                            supportChats.remove(player);
                            return true;
                        }
                    case "help":
                        player.sendMessage(Utils.color("&8<>[==- &9/ticket help &8-==]<>"));
                        player.sendMessage(Utils.color("&9/ticket help &7- Zeigt diese Liste"));
                        player.sendMessage(Utils.color("&9/ticket list &7- Listet die aktuelle Warteschlange auf"));
                        player.sendMessage(Utils.color("&9/ticket accept <Spieler> &7- Akzeptiert eine Anfrage und �ffnet den Chat"));
                        player.sendMessage(Utils.color("&9/ticket deny <Spieler> &7- Lehnt eine Anfrage ab"));
                        player.sendMessage(Utils.color("&9/ticket close [Grund] &7- Schliesst den aktiven Chat"));
                        player.sendMessage(Utils.color("&9/ticket spy &7- Zeigt alle Chats"));
                        break;
                    default:
                        player.sendMessage(Utils.color("&7/support help"));
                        return true;
                }
            } else if(args.length == 2) {
                String command = args[0].toLowerCase();
                Player target = Bukkit.getPlayer(args[1]);

                if(target == null) {
                    player.sendMessage(Utils.color("&7Dieser Spieler ist nicht online."));
                    return true;
                }

                switch(command) {
                    case "accept":

                        if(!playerQueue.contains(target)) {
                            player.sendMessage(Utils.color("&7Dieser Spieler befindet sich nicht in der Warteschlange!"));
                            return true;
                        } else {
                            sendTeam("&9"+player.getName()+" &7befindet sich nun im Chat mit &9"+target.getName());
                            target.sendMessage(Utils.color("&7Du befindest dich nun im Chat mit &9"+player.getName()+" &7!"));
                            supportChats.put(player, target);
                            playerQueue.remove(target);
                            return true;
                        }

                    case "deny":
                        if(!playerQueue.contains(target)) {
                            player.sendMessage(Utils.color("&7Dieser Spieler befindet sich nicht in der Warteschlange!"));
                            return true;
                        } else {
                            sendTeam("&9"+player.getName()+" &7hat &9"+target.getName()+"'s &7Anfrage abgelehnt!");
                            target.sendMessage(Utils.color("&7Deine Anfrage wurde von &9"+player.getName()+" &7abgelehnt!"));
                            playerQueue.remove(target);
                            return true;
                        }
                    default: break;
                }
            }
        }
        return true;
    }


    @EventHandler(priority= EventPriority.NORMAL)
    public void onPlayerChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();

        if(supportChats.containsKey(player) || supportChats.containsValue(player)) {
            event.setCancelled(true);
            String format = "&9&lTICKET &7» %player&7: &e%m";
            if(supportChats.containsKey(player)) {

                String message = format
                        .replaceAll("%player", (player.hasPermission("vorista.ticket.staff") ? "&cSTAFF &7": "")+player.getName())
                        .replaceAll("%m", event.getMessage());
                String spyMessage = format
                        .replaceAll("%player", player.getName()+" -> "+supportChats.get(player).getName())
                        .replaceAll("%m", event.getMessage());


                player.sendMessage(Utils.color(message));
                supportChats.get(player).sendMessage(Utils.color(message));

                // Spy's senden
                for(Player p : spies) {
                    p.getPlayer().sendMessage(Utils.color(spyMessage));
                }

                log(Utils.stripColor(spyMessage));

            } else {
                for(Player key : supportChats.keySet()) {
                    if(supportChats.get(key) == player) {

                        String message = format.replaceAll("%player", player.getName())
                                .replaceAll("%m", event.getMessage());
                        String spyMessage = format
                                .replaceAll("%player", player.getName()+" -> "+key.getName())
                                .replaceAll("%m", event.getMessage());

                        player.sendMessage(Utils.color(message));
                        key.sendMessage(Utils.color(message));

                        // Spy's senden
                        for(Player p : spies) {
                            p.getPlayer().sendMessage(Utils.color(spyMessage));
                        }

                        log(Utils.stripColor(spyMessage));

                    }
                }
            }
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        if(supportChats.containsKey(player) || supportChats.containsValue(player)) {

            if(supportChats.containsKey(player)) {
                supportChats.get(player).sendMessage(Utils.color("&7Dein Chatpartner hat die Verbindung getrennt!"));
                supportChats.get(player).sendMessage(Utils.color("&7Der Chat wurde geschlossen."));
                supportChats.remove(player);
                return;
            } else {
                for(Player key : supportChats.keySet()) {
                    if(supportChats.get(key) == player) {
                        key.sendMessage(Utils.color("&7Dein Chatpartner hat die Verbindung getrennt!"));
                        key.sendMessage(Utils.color("&7Der Chat wurde geschlossen!"));
                        supportChats.remove(key);
                        return;
                    }
                }
            }
        }
    }

    private void log(String msg) {
        File dataFolder = VoristaPlugin.getInstance().getDataFolder();
        if(!dataFolder.exists()) {
            dataFolder.mkdir();
        }

        File logFile = new File(VoristaPlugin.getInstance().getDataFolder(), "ticketLog.txt");
        if(!logFile.exists()) {
            try {
                logFile.createNewFile();
            } catch(IOException ex) {
                ex.printStackTrace();
            }
        }
        GregorianCalendar calendar = new GregorianCalendar();
        String timeStamp = "<"+calendar.get(Calendar.DAY_OF_MONTH)
                +". "+calendar.get(Calendar.MONTH)
                +". "+calendar.get(Calendar.HOUR_OF_DAY)
                +":"+calendar.get(Calendar.MINUTE)
                +":"+calendar.get(Calendar.SECOND)+">";
        FileWriter fw;
        PrintWriter pw;
        try {
            fw = new FileWriter(logFile, true);
            pw = new PrintWriter(fw);

            pw.println(timeStamp+" "+msg);
            pw.flush();
            pw.close();
        } catch (IOException e) { e.printStackTrace(); }

    }

    private void sendTeam(String msg) {
        Bukkit.getOnlinePlayers().stream().filter(p -> p.hasPermission("vorista.ticket.staff")).forEach(p -> {
            p.sendMessage(Utils.color(msg));
        });
    }

}
