package de.vorista.Vorista.commands;

import de.vorista.Vorista.VoristaPlugin;
import de.vorista.Vorista.utils.CustomCmdManager;
import de.vorista.Vorista.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Corvin on 06.10.2016.
 */
public class CmdCustomCmd implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(!(sender instanceof Player)) {
            return true;
        }

        Player player = (Player) sender;

        if(!player.hasPermission("vorsita.customcmd.modify")) {
            player.sendMessage(Utils.color("&7[&9V&7] Keine Permission"));
            return true;
        }

        CustomCmdManager manager = VoristaPlugin.getCustomCommand();

        if(args.length == 0) {
            Bukkit.dispatchCommand(player, "customcmd help");
            return true;
        } else {
            String subcommand = args[0];

            if(subcommand.equalsIgnoreCase("create")) {
                // /customcmd create <cmd>
                if(args.length != 2) {
                    Bukkit.dispatchCommand(player, "customcmd help");
                    return true;
                }
                player.sendMessage(Utils.color("&7[&9V&7] "+
                        (manager.createCommand(args[1]) ? "Der Befehl '"+args[1]+"' wurde hinzugefügt.":
                                "Diesen Befehl gibt es bereits!")));
                return true;
            }

            else if(subcommand.equalsIgnoreCase("add")) {
                // /customcmd add <cmd> <line>
                if(args.length < 3) {
                    Bukkit.dispatchCommand(player, "customcmd help");
                    return true;
                }
                String line = "";
                for(int i = 2; i < args.length; i++) line += " " + args[i];
                line = line.replaceFirst(" ", "");

                player.sendMessage(Utils.color("&7[&9V&7] "+
                        (manager.addToCommand(args[1], Arrays.asList(line)) ? "Zeile wurde zum Befehl hinzugefügt.":
                                "Diesen Befehl gibt es nicht!")));
                return true;
            }

            else if(subcommand.equalsIgnoreCase("remove")) {
                // /customcmd remove <cmd>
                if(args.length != 2) {
                    Bukkit.dispatchCommand(player, "customcmd help");
                    return true;
                }

                player.sendMessage(Utils.color("&7[&9V&7] "+
                        (manager.removeCommand(args[1]) ? "Befehl wurde gelöscht.":
                                "Diesen Befehl gibt es nicht!")));
                return true;
            }

            else if(subcommand.equalsIgnoreCase("removeline")) {
                // /customcmd removeline <cmd> <index>
                if(args.length != 3) {
                    Bukkit.dispatchCommand(player, "customcmd help");
                    return true;
                }
                int index = -1;
                try {
                    index = Integer.parseInt(args[2]);
                } catch(NumberFormatException ex) {
                    Bukkit.dispatchCommand(player, "customcmd help");
                    return true;
                }
                player.sendMessage(Utils.color("&7[&9V&7] "+
                        (manager.removeFromCommand(args[1], index) ? "Zeile wurde gelöscht.":
                                "Diesen Befehl gibt es nicht!")));
                return true;
            }

            else if(subcommand.equalsIgnoreCase("list")) {
                // /customcmd list
                if(args.length != 1) {
                    Bukkit.dispatchCommand(player, "customcmd help");
                    return true;
                }
                int items = 0;
                player.sendMessage(Utils.color("&7-----<= &9Custom-Befehle &7=>-----"));
                for(String cmd : manager.getCustomCommands()) {
                    items++;
                    player.sendMessage(Utils.color("&7[&9"+items+"&7] &f"+cmd));
                }
                if(items == 0) player.sendMessage(Utils.color("&7&oKeine Befehle gefunden."));

                return true;
            }

            else if(subcommand.equalsIgnoreCase("listlines")) {
                // /customcmd listlines <cmd>
                if(args.length != 2) {
                    Bukkit.dispatchCommand(player, "customcmd help");
                    return true;
                }
                List<String> lines = manager.getLinesOf(args[1]);
                if(lines == null) { player.sendMessage(Utils.color("&7[&9V&7] Dieser Befehl existiert nicht!")); return true; }
                int items = -1;
                player.sendMessage(Utils.color("&7-----<= &9Zeilen von &o"+args[1]+" &7=>-----"));

                for(String line : lines) {
                    items++;
                    player.sendMessage(Utils.color("&7[&9"+items+"&7] &f"+line));
                }
                if(items == -1) player.sendMessage(Utils.color("&7&oKeine Zeilen gefunden."));

                return true;
            }

            else if(subcommand.equalsIgnoreCase("reload")) {
                // /customcmd reload
                if(args.length != 1) {
                    Bukkit.dispatchCommand(player, "customcmd help");
                    return true;
                }
                manager.reload();
                player.sendMessage(Utils.color("&7[&9V&7] CustomCommands.yml reloaded."));
                return true;
            }

            else if(subcommand.equalsIgnoreCase("help")) {
                List<String> helpLines = Arrays.asList(
                        "&7-----<= &9Hilfe für /customcmd oder /ccmd &7=>-----",
                        "&9/ccmd create <command> &7- Erstelle einen neuen Command",
                        "&9/ccmd add <cmd> <line> &7- Füge einem Befehl eine Zeile hinzu",
                        "&9/ccmd listlines <cmd> &7- Liste die Zeilen eines Commands auf",
                        "&9/ccmd removeline <cmd> <index> &7- Lösche die Zeile <index> vom Command",
                        "&9/ccmd remove <cmd> &7- Lösche einen Command",
                        "&9/ccmd list &7- Liste alle Commands auf",
                        "&9/ccmd reload &7- Lade die customCommands.yml neu",
                        "&9/ccmd help &7- Zeigt diese Hilfe an");

                for(String s : helpLines) player.sendMessage(Utils.color(s));
                return true;
            }
        }
        return true;
    }

}
