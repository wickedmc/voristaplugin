package de.vorista.Vorista.commands;

import de.vorista.Vorista.AutoMessenger;
import de.vorista.Vorista.VoristaPlugin;
import de.vorista.Vorista.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Corvin on 22.10.2016.
 */
public class CmdAutomessenger implements CommandExecutor {

    private AutoMessenger autoMessenger;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(!(sender instanceof Player)) return true;

        Player player = (Player) sender;
        autoMessenger = VoristaPlugin.getInstance().getAutoMessenger();

        if(!player.hasPermission("vorista.smod")) {
            player.sendMessage(Utils.color("&7[&9V&7] Du hast keinen Zugriff auf diesen Befehl!"));
            return true;
        }

        if(args.length == 0) {
            Bukkit.dispatchCommand(player, "am help");
            return true;
        }

        String subcommand = args[0].toLowerCase();

        switch(subcommand) {
            case "list":
                player.sendMessage(Utils.color("&9--------------- AM Messages ---------------"));
                    for(int i = 0; i < autoMessenger.getMessages().size(); i++) {
                        player.sendMessage(Utils.color("&7[&9"+i+"&7] &r"+autoMessenger.getMessages().get(i)));
                    }
                player.sendMessage(Utils.color("&9------------------------------------------"));
                return true;
            case "interval":
                if(args.length == 1) {
                    player.sendMessage(Utils.color("&7[&9V&7] Aktueller Intervall: &r"+autoMessenger.getMinuteInterval()));
                    return true;
                }
                if(args.length != 2) break;
                int interval = -1;
                try {
                    interval = Integer.parseInt(args[1]);
                } catch(NumberFormatException ex) { break; }
                autoMessenger.setMinuteInterval(interval);
                player.sendMessage(Utils.color("&7[&9V&7] Intervall zu &f"+autoMessenger.getMinuteInterval()+"&7 gesetzt."));
                return true;
            case "prefix":
                if(args.length == 1) {
                    player.sendMessage(Utils.color("&7[&9V&7] Aktuelles Prefix: &r"+autoMessenger.getPrefix()));
                    return true;
                }
                if(args.length < 2) break;
                String prefix = "";
                for(int i = 1; i < args.length; i++) prefix += " "+args[i];
                prefix = prefix.replaceFirst(" ", "");
                autoMessenger.setPrefix(prefix);
                player.sendMessage(Utils.color("&7[&9V&7] Prefix gesetzt zu: &r"+autoMessenger.getPrefix()));
                return true;
            case "start":case "stop":
                if(args.length != 2) { break; }
                boolean start = args[1].equalsIgnoreCase("start");
                if(start) autoMessenger.start();
                else autoMessenger.stop();
                player.sendMessage(Utils.color("&7[&9V&7] Automessenger "+(start ? "gestartet.":"gestoppt.")));
                return true;
            case "remove":
                if(args.length != 2) { break; }
                int index = -1;
                try {
                    index = Integer.parseInt(args[1]);
                } catch(NumberFormatException ex) { break; }
                autoMessenger.removeMessage(index);
                player.sendMessage(Utils.color("&7[&9V&7] Nachricht gelöscht."));
                return true;
            case "add":
                if(args.length < 2) { break; }
                String message = "";
                for(int i = 1; i < args.length; i++) message += " "+args[i];
                message = message.replaceFirst(" ", "");
                autoMessenger.addMessage(message);
                player.sendMessage(Utils.color("&7[&9V&7] Nachricht hinzigefügt."));
                return true;
            case "reload":
                autoMessenger.reload();
                player.sendMessage(Utils.color("&7[&9V&7] Messages wurden reloaded."));
                return true;
            case "help": default:
                List<String> helpMessages = Arrays.asList(
                        "&7-----<= &9Hilfe für /automessenger oder /am &7=>-----",
                        "&9/am list &7- Zeigt aktuelle Messages an",
                        "&9/am interval [x] &7- Zeigt und setzt den Interval",
                        "&9/am prefix [x] &7- Zeigt und setzt das Prefix",
                        "&9/am start/stop &7- Startet und stoppt den Thread",
                        "&9/am remove <index> &7- Löscht eine Nachricht",
                        "&9/am add <message> &7- Fügt eine Nachricht hinzu",
                        "&9/am reload &7- Lädt die Messagefile neu");

                for(String s : helpMessages) player.sendMessage(Utils.color(s));

                return true;
        }
        Bukkit.dispatchCommand(player, "am help");
        return true;
    }
}
