package de.vorista.Vorista.commands;

import de.vorista.Vorista.VoristaPlugin;
import de.vorista.Vorista.utils.Tag;
import de.vorista.Vorista.utils.Utils;
import de.vorista.Vorista.votecrates.VoteCrate;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Corvin on 09.10.2016.
 */
public class CmdVorista implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(!(sender instanceof Player)) return true;

        Player player = (Player) sender;

        if(!player.hasPermission("vorista.maincmd")){
            player.sendMessage(Utils.color("&7[&9V&7] Vielen Dank, dass du auf Vorista spielst! Bei Fragen und Problemen kannst du gerne /ticket benutzen."));
            return true;
        }

        if(args.length == 0) {
            Bukkit.dispatchCommand(player, "vorista reload");
            return true;
        }

        String subcommand = args[0];

        if(subcommand.equalsIgnoreCase("reload")) {
            player.sendMessage(Utils.color("&7[&9V&7] Reloade..."));
            VoristaPlugin.getInstance().reloadConfig();
            Tag.reload();
            VoteCrate.reload();
            player.sendMessage(Utils.color("&7[&9V&7] &aReload fertig."));
            return true;
        }

        if(subcommand.equalsIgnoreCase("settings") || subcommand.equalsIgnoreCase("s")) {
            // /vorista s cmd value

            player.sendMessage(Utils.color("&7[&9V&7] Keine Einstellungen zu setzen."));

            return true;
        }

        if(subcommand.equalsIgnoreCase("uuid")) {
            Player target = Bukkit.getPlayer(args[1]);
            if(target == null) {
                player.sendMessage(Utils.color("&7[&9V&7] Spieler nicht gefunden."));
                return true;
            }
            player.sendMessage(Utils.color("&7[&9V&7] UUID von "+target.getName()+" ist &f"+target.getUniqueId().toString()));
        }

        return true;
    }

}
