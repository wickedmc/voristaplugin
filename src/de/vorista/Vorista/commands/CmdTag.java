package de.vorista.Vorista.commands;

import de.vorista.Vorista.utils.Tag;
import de.vorista.Vorista.utils.TagMenu;
import de.vorista.Vorista.utils.Utils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Corvin on 08.10.2016.
 */
public class CmdTag implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(!(sender instanceof Player)) return true;

        Player player = (Player) sender;

        if(args.length == 1 && args[0].equalsIgnoreCase("reload") && player.hasPermission("vorista.tags.reload")) {
            Tag.reload();
            player.sendMessage(Utils.color("&7[&9V&7] Tags wurden neu geladen."));
            return true;
        }

        TagMenu.show(player);
        player.sendMessage(Utils.color("&7[&9V&7] Tag Menu wird geöffnet..."));

        return true;
    }
}
