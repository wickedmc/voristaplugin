package de.vorista.Vorista.commands;

import de.vorista.Vorista.utils.ItemStackBuilder;
import de.vorista.Vorista.utils.Utils;
import de.vorista.Vorista.votecrates.VoteCrate;
import de.vorista.Vorista.votecrates.VoteReward;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Corvin on 01.10.2016.
 */
public class CmdVotecrate implements CommandExecutor {

    private final String helpCommand = "vc help";

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(!(sender instanceof Player)) return true;

        Player player = (Player) sender;

        if (!player.hasPermission("vorsita.votecrate.manage")) {
            player.sendMessage(Utils.color("&7[&9V&7] Keine Permission"));
            return true;
        }

        if(args.length == 0) {
            Bukkit.dispatchCommand(player, helpCommand);
            return true;
        }

        String subcommand = args[0];

        if(subcommand.equalsIgnoreCase("item")) {
            // /vc item
            if (args.length != 1) {
                Bukkit.dispatchCommand(player, helpCommand);
                return true;
            }
            player.getInventory().addItem(VoteCrate.getCrateItem());
            player.updateInventory();
            player.sendMessage(Utils.color("&7[&9V&7] Votecrate Item gegeben."));
            return true;
        }

        else if(subcommand.equalsIgnoreCase("key")) {
            // /vc key <amount>
            if (args.length < 1 || args.length > 2) {
                Bukkit.dispatchCommand(player, helpCommand);
                return true;
            }
            int amount = 1;
            if(args.length == 2) {
                try {
                    amount = Integer.parseInt(args[1]);
                } catch(Exception ex) {
                    player.sendMessage(Utils.color("&7[&9V&7] Bitte gib eine Zahl an. /vc key <amount>"));
                }
            }
            player.getInventory().addItem(new ItemStackBuilder(VoteCrate.getKey()).withAmount(amount).build());
            player.updateInventory();
            player.sendMessage(Utils.color("&7[&9V&7] "+amount+" Keys gegeben."));
            return true;
        }

        else if(subcommand.equalsIgnoreCase("list") || subcommand.equalsIgnoreCase("rewards")) {
            // /vc list
            if (args.length != 1) {
                Bukkit.dispatchCommand(player, helpCommand);
                return true;
            }
            player.sendMessage(Utils.color("&9--------------- VoteRewards ---------------"));
            int item = 0;
            for(VoteReward reward : VoteCrate.getRewards()) {
                String listItem;
                if(reward.isCommand()) {
                    listItem = "&7[&9"+item+"&7] &f"+reward.getName()
                            +"&7: Command: &f"+reward.getCommand()
                            +" &7Chance: &f"+reward.getChance();
                } else {
                    listItem = "&7[&9"+item+"&7] &f"+reward.getName()
                            +" &7Amount:  &f"+reward.getItem().getAmount()
                            +" &7Chance: &f"+reward.getChance();
                }
                player.sendMessage(Utils.color(listItem));
                item++;
            }
            player.sendMessage(Utils.color("&9------------------------------------------"));

            return true;
        }

        else if(subcommand.equalsIgnoreCase("add") || subcommand.equalsIgnoreCase("addreward")) {
            // /vc add <chance> <name>
            if (args.length < 3) {
                Bukkit.dispatchCommand(player, helpCommand);
                return true;
            }

            if(player.getItemInHand() == null || player.getItemInHand().getType() == Material.AIR) {
                player.sendMessage(Utils.color("&7[&9V&7] Bitte nimm ein Item in die Hand."));
                return true;
            }

            double chance = 0;
            try {
                chance = Double.parseDouble(args[1]);
            } catch(Exception ex) {
                player.sendMessage(Utils.color("&7[&9V&7] Bitte gib eine Zahl an. /vc add <chance>"));
                return true;
            }

            String name = "";
            for(int i = 2; i < args.length; i++) name += " " + args[i];
            name = name.replaceFirst(" ", "");

            VoteReward reward;
            if(player.getItemInHand().getType() == Material.BOOK) {
                reward = new VoteReward(name, null, player.getItemInHand().getItemMeta().getDisplayName(), chance);
            } else {
                reward = new VoteReward(name, new ItemStack(player.getItemInHand()), null, chance);
            }

            VoteCrate.addReward(reward);
            player.sendMessage(Utils.color("&7[&9V&7] Reward hinzugefügt. Chance: &f"+chance));
            return true;
        }

        else if(subcommand.equalsIgnoreCase("set") || subcommand.equalsIgnoreCase("setreward")) {
            // /vc set <index> <chance> <name>
            if (args.length < 4) {
                Bukkit.dispatchCommand(player, helpCommand);
                return true;
            }

            if(player.getItemInHand() == null || player.getItemInHand().getType() == Material.AIR) {
                player.sendMessage(Utils.color("&7[&9V&7] Bitte nimm ein Item in die Hand."));
                return true;
            }

            double chance = -1;
            int index = -1;
            try {
                chance = Double.parseDouble(args[2]);
                index = Integer.parseInt(args[1]);
            } catch(Exception ex) {
                player.sendMessage(Utils.color("&7[&9V&7] Bitte gib eine Zahl an. /vc set <index> <chance>"));
                return true;
            }

            String name = "";
            for(int i = 3; i < args.length; i++) name += " " + args[i];
            name = name.replaceFirst(" ", "");

            VoteReward reward;
            if(player.getItemInHand().getType() == Material.BOOK) {
                reward = new VoteReward(name, null, player.getItemInHand().getItemMeta().getDisplayName(), chance);
            } else {
                reward = new VoteReward(name, new ItemStack(player.getItemInHand()), null, chance);
            }
            VoteCrate.setReward(reward, index);
            player.sendMessage(Utils.color("&7[&9V&7] Reward gesetzt. Chance: &f"+chance));
            return true;
        }

        else if(subcommand.equalsIgnoreCase("remove") || subcommand.equalsIgnoreCase("removereward")) {
            // /vc remove <index>
            if (args.length != 2) {
                Bukkit.dispatchCommand(player, helpCommand);
                return true;
            }
            int index = 0;
            try {
                index = Integer.parseInt(args[1]);
            } catch(Exception ex) {
                player.sendMessage(Utils.color("&7[&9V&7] Bitte gib eine Zahl an. /vc remove <index>"));
                return true;
            }

            VoteCrate.removeReward(index);
            player.sendMessage(Utils.color("&7[&9V&7] Reward gelöscht"));
            return true;
        }

        else if(subcommand.equalsIgnoreCase("animatetext") || subcommand.equalsIgnoreCase("atxt")) {
            // /vc atxt <boolean>
            if (args.length != 2) {
                Bukkit.dispatchCommand(player, helpCommand);
                return true;
            }
            boolean animate = VoteCrate.isAnimateText();
            try {
                animate = Boolean.parseBoolean(args[1]);
            } catch(Exception ex) {
                player.sendMessage(Utils.color("&7[&9V&7] Bitte gib true/false an. Aktuell: &f"+animate));
                return true;
            }

            VoteCrate.setAnimateText(animate);
            player.sendMessage(Utils.color("&7[&9V&7] "+ (animate ? "Text wird jetzt animiert.":"Text wird nicht mehr animiert.")));
            return true;
        }

        else if(subcommand.equalsIgnoreCase("useparticles") || subcommand.equalsIgnoreCase("usep")) {
            // /vc usep <boolean>
            if (args.length != 2) {
                Bukkit.dispatchCommand(player, helpCommand);
                return true;
            }
            boolean use = VoteCrate.isUseParticles();
            try {
                use = Boolean.parseBoolean(args[1]);
            } catch(Exception ex) {
                player.sendMessage(Utils.color("&7[&9V&7] Bitte gib true/false an. Aktuell: &f"+use));
                return true;
            }

            VoteCrate.setUseParticles(use);
            player.sendMessage(Utils.color("&7[&9V&7] "+ (use ? "Partikel werden benutzt.":"Partikel werden nicht mehr benutzt.")));
            return true;
        }

        else if(subcommand.equalsIgnoreCase("particle") || subcommand.equalsIgnoreCase("p") || subcommand.equalsIgnoreCase("effect")) {
            // /vc p <effect>
            if (args.length != 2) {
                Bukkit.dispatchCommand(player, helpCommand);
                return true;
            }
            Effect effect = VoteCrate.getParticleEffect();
            try {
                effect = Effect.valueOf(args[1].toUpperCase());
            } catch(Exception ex) {
                player.sendMessage(Utils.color("&7[&9V&7] Bitte gib einen Effect an an. Aktuell: &f"+effect.name()));
                return true;
            }

            VoteCrate.setParticleEffect(effect);
            player.sendMessage(Utils.color("&7[&9V&7] Partikeleffekt zu &f"+effect.name()+" &7gesetzt."));
            return true;
        }

        else if(subcommand.equalsIgnoreCase("counter")) {
            player.sendMessage(Utils.color("&7[&9V&7] Aktueller Counter: &f"+VoteCrate.getCounter()));
        }

        else if(subcommand.equalsIgnoreCase("effects")) {
            player.sendMessage(Utils.color("&7[&9V&7] Effecte: "));
            List<String> effects = new ArrayList<>();
            for(Effect effect : Effect.values()) {
                effects.add(effect.name());
            }
            player.sendMessage(Utils.commaList(effects));
            return true;
        }

        else if(subcommand.equalsIgnoreCase("reload")) {
            // /vc reload
            if (args.length != 1) {
                Bukkit.dispatchCommand(player, helpCommand);
                return true;
            }
            VoteCrate.reload();
            player.sendMessage(Utils.color("&7[&9V&7] Crates und Rewards neu geladen."));
            return true;
        }

        else if(subcommand.equalsIgnoreCase("help")) {
            List<String> helpLines = Arrays.asList(
                    "&7-----<= &9Hilfe für /votecrate oder /vc &7=>-----",
                    "&9/vc item &7- Gibt dir das Item um eine Crate zu setzen",
                    "&9/vc key <x> &7- Gibt dir x Keys",
                    "&9/vc list &7- Listet die Aktuellen Rewards auf",
                    "&9/vc add <chance> <name> &7- Fügt das Item in deiner Hand zu den Rewards hinzu",
                    "&9/vc set <index> <chance> <name> &7- Setzt einen Reward",
                    "&9/vc remove <index> &7- Löscht <index> aus den Rewards",
                    "&9/vc reload &7- Lädt die Rewards und Votecrates neu",
                    "&9/vc help &7- Zeigt diese Hilfe an");

            for(String s : helpLines) player.sendMessage(Utils.color(s));
        }

        return true;
    }

}
