package de.vorista.Vorista.commands;

import de.vorista.Vorista.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Corvin on 16.10.2016.
 */
public class CmdAdminalert implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (!(sender instanceof Player)) return true;

        Player player = (Player) sender;

        if (!player.hasPermission("vorista.admin")) {
            player.sendMessage(Utils.color("&7[&9V&7] Keine Rechte."));
            return true;
        }

        String prefix = "";

        if(label.equalsIgnoreCase("alert")) {
            prefix = "&4&lSERVERMELDUNG &c&k||&r &6";
        }

        String message = "";
        for(String s : args) message += " " + s;
        message = message.replaceFirst(" ", "");

        Bukkit.broadcastMessage(Utils.color(prefix+message));

        return true;
    }
}
