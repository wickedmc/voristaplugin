package de.vorista.Vorista.utils;

import de.vorista.Vorista.VoristaPlugin;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;

/**
 * Created by Corvin on 14.09.2016.
 */
public class Messages {

    private File messageFile;
    private FileConfiguration config;

    private final String DEFAULT_MESSAGE = "Missing message at ";

    public Messages(Plugin plugin) {
        messageFile = new File(plugin.getDataFolder(), "messages.yml");

        if(!messageFile.exists()) {
            try {
//                Utils.ExportResource("/src/messages.yml");
                messageFile.createNewFile();
            } catch (Exception e) {
                VoristaPlugin.debug("Error copying message file: "+e.getMessage());
            }
        }

        config = YamlConfiguration.loadConfiguration(messageFile);
        saveFile();
    }

    public String getMessage(String path) {
        String msg = config.getString(path);
        if(msg == null) { config.set(path, DEFAULT_MESSAGE+path); saveFile(); }
        return Utils.color(config.getString(path));
    }

    public String getMessage(String path, String... args) {
        String msg = config.getString(path);
        String arg = "";
        for(String s : args) {
            arg += ", "+s;
        }
        if(msg == null) { config.set(path, DEFAULT_MESSAGE+path+". args:"+arg.replaceFirst(", ", "")); saveFile(); }
        return Utils.color(String.format(config.getString(path), args));
    }

    public void reloadFile() {
        saveFile();
        config = YamlConfiguration.loadConfiguration(messageFile);
    }

    public void saveFile() {
        try {
            config.save(messageFile);
        } catch (IOException e) {
            VoristaPlugin.debug("Error saving message file: "+e.getMessage());
        }
    }

}
