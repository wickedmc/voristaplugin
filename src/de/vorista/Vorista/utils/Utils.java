package de.vorista.Vorista.utils;

import org.bukkit.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Created by Corvin on 14.09.2016.
 */
public class Utils {

    /**
     * Color a string with ChatColors
     * @param s String to translate colors of
     * @return ChatColored string
     */
    public static String color(String s) { return ChatColor.translateAlternateColorCodes('&', s); }
    public static String stripColor(String s) { return ChatColor.stripColor(s); }

    public static void broadcast(String perm, String msg) {
        Bukkit.getOnlinePlayers().stream().filter(player -> player.hasPermission(perm)).forEach(player -> player.sendMessage(color(msg)));
    }

    public static void broadcast(Collection<Player> players, String msg) {
        for(Player player : players) player.sendMessage(color(msg));
    }

    public static String commaList(List<String> list) {
        String builder = "";
        for(String s : list) {
            builder += ", "+s;
        }
        return builder.replaceFirst(", ", "");
    }

    public static boolean compareItems(ItemStack a, ItemStack b, boolean countEnchants) {
        boolean type;
        boolean damage;
        boolean name;
        boolean enchants = true;

        if(!a.hasItemMeta() || !b.hasItemMeta()) name = false;
        else {
            ItemMeta metaA = a.getItemMeta();
            ItemMeta metaB = b.getItemMeta();
            name = (metaA.hasDisplayName() && metaB.hasDisplayName()) && (metaA.getDisplayName().equalsIgnoreCase(metaB.getDisplayName()));
        }

        type = a.getType().equals(b.getType());
        damage = a.getDurability() == b.getDurability();
        if(countEnchants) enchants = a.getEnchantments().equals(b.getEnchantments());

        return type && damage && name && enchants;
    }

    public static String locationToString(Location loc, boolean pitch) {
        return loc.getWorld().getName()+" "+loc.getX()+"/"+loc.getY()+"/"+loc.getZ()+(pitch ? (loc.getPitch()+"/"+loc.getYaw()):"");
    }

    public static Location locationFromString(String s, boolean _pitch) {
        String[] args = s.split(" ");
        World world = Bukkit.getWorld(args[0]);
        double x = Double.parseDouble(args[1].split("/")[0]);
        double y = Double.parseDouble(args[1].split("/")[1]);
        double z = Double.parseDouble(args[1].split("/")[1]);
        if(_pitch) {
            float pitch = Float.parseFloat(args[2].split("/")[0]);
            float yaw = Float.parseFloat(args[2].split("/")[1]);
            return new Location(world, x, y, z, yaw, pitch);
        }
        return new Location(world, x, y, z);
    }

    public static String itemStackToString(ItemStack itemStack) {
        // type data durability amount enchant:level... displayname lore...
        String itemString = "";

        itemString += "type:"+itemStack.getType().name() + " ";
        itemString += "dura:"+itemStack.getDurability() + " ";
        itemString += "amount:"+itemStack.getAmount();

        if(!itemStack.getEnchantments().isEmpty()) {
            String enchants = " enchants:";
            for(Map.Entry<Enchantment, Integer> enchantment : itemStack.getEnchantments().entrySet())
                enchants += ","+enchantment.getKey().getName()+"/"+enchantment.getValue();
            itemString += enchants.replaceFirst(",", "");
        }

        if(itemStack.hasItemMeta()) {
            ItemMeta meta = itemStack.getItemMeta();
            if(meta.hasDisplayName()) {
                itemString += " name:"+meta.getDisplayName().replaceAll(" ", "_")+"";
            }
            if(meta.hasLore()) {
                String lore = " lore:";
                for(String s : meta.getLore())
                    lore += ","+s.replaceAll(" ","_")+"";
                itemString += lore.replaceFirst(",", "");
            }
        }

        return itemString;
    }

    public static ItemStack itemStackFromString(String itemString) {
        ItemStack itemStack = new ItemStack(Material.AIR);

        for(String s : itemString.split(" ")) {
            String value;

            if(s.startsWith("type:")) {
                value = s.replaceFirst("type:", "");
                itemStack.setType(Material.valueOf(value.toUpperCase()));
            }

            else if(s.startsWith("dura:")) {
                value = s.replaceFirst("dura:", "");
                itemStack.setDurability(Short.parseShort(value));
            }

            else if(s.startsWith("amount:")) {
                value = s.replaceFirst("amount:", "");
                itemStack.setAmount(Integer.parseInt(value));
            }

            else if(s.startsWith("enchants:")) {
                value = s.replaceFirst("enchants:", "");
                for(String ench : value.split(","))
                    itemStack.addUnsafeEnchantment(Enchantment.getByName(ench.split("/")[0]), Integer.parseInt(ench.split("/")[1]));
            }

            else if(s.startsWith("name:")) {
                value = s.replaceFirst("name:", "");
                ItemMeta meta = itemStack.getItemMeta();
                meta.setDisplayName(value.replaceAll("_", " "));
                itemStack.setItemMeta(meta);
            }

            else if(s.startsWith("lore:")) {
                value = s.replaceFirst("lore:", "");
                ItemMeta meta = itemStack.getItemMeta();
                List<String> lore = new ArrayList<>();
                for(String line : value.split(","))
                    lore.add(line.replaceAll("_", " "));
                meta.setLore(lore);
                itemStack.setItemMeta(meta);
            }

        }
        return itemStack;
    }
}
