package de.vorista.Vorista.utils;

import de.vorista.Vorista.VoristaPlugin;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Corvin on 06.10.2016.
 */
public class Tag {

    private static Map<String, Tag> tags = new HashMap<>();

    private static File file;
    private static FileConfiguration config;

    static {
        file = new File(VoristaPlugin.getInstance().getDataFolder(), "tags.yml");

        if(!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        config = YamlConfiguration.loadConfiguration(file);
    }

    private String name;
    private String prefix;
    private char color;
    private String permission;

    public Tag(String name, String prefix, char color, String permission, boolean save) {
        this.name = name;
        this.prefix = prefix;
        this.color = color;
        this.permission = permission;

        tags.put(name, this);
        if(save) saveToFile();
    }

    private void saveToFile() {
        List<String> tagList = config.getStringList("tags");
        tagList.add("name:"+name+" "
                    + "prefix:"+prefix.replaceAll(" ", "_")+" "
                    + "color:"+color+" "
                    + "permission:"+permission);
        config.set("tags", tagList);
    }

    public String getName() { return name; }
    public String getPrefix() { return prefix; }
    public char getColor() { return color; }
    public String getPermission() { return permission; }

    public ItemStack getItem(Player player) {
        ItemStackBuilder builder = new ItemStackBuilder(Material.NAME_TAG)
                .withName("&r"+name)
                .withLore(Utils.color(prefix));
        if(player != null) {
            Tag tag = getActiveTagOf(player);
            builder.withLore(tag != null && tag.getName().equals(name) ? "&aAktiviert":"&7&oKlicken zum Auswählen");
        }
        return builder.build();
    }

    public static Map<String, Tag> getTags() { return tags; }

    public static void setActiveTagOf(Player player, String tag) {
        if(tag == null) config.set(player.getUniqueId().toString(), null);
        else config.set(player.getUniqueId().toString(), tags.get(tag).getName());
        saveFile();
    }

    public static Tag getActiveTagOf(Player player) {
        if(config.contains(player.getUniqueId().toString()))
            return tags.get(config.getString(player.getUniqueId().toString()));
        else
            return null;
    }

    public static Tag getTag(String name) {
        for(String tagName : tags.keySet()) if (tagName.equalsIgnoreCase(name)) return tags.get(tagName);
        return null;
    }

    public static void loadTags() {
        Bukkit.getLogger().info("Starting loading of tags...");
        List<String> tagList = config.getStringList("tags");
        Bukkit.getLogger().info("List size: "+tagList.size());
        for(String tag : tagList) {

            String name = "";
            String prefix = "";
            char color = '7';
            String permission = "";

            for(String s : tag.split(" ")) {
                if(s.startsWith("name:")) name = s.replaceFirst("name:", "");

                if(s.startsWith("prefix:")) prefix = s.replaceFirst("prefix:", "");

                if(s.startsWith("color:")) color = s.replaceFirst("color:", "").toCharArray()[0];

                if(s.startsWith("permission:")) permission = s.replaceFirst("permission:", "");
            }
            Bukkit.getLogger().info("Tag loaded: "+name+"/"+prefix+"/"+permission);
            new Tag(name, prefix, color, permission, false);
        }
    }

    public static void reload() {
        config = YamlConfiguration.loadConfiguration(file);
        tags.clear();
        loadTags();
    }

    public static void saveTags() { tags.values().forEach(Tag::saveToFile); }

    private static void saveFile() {
        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
