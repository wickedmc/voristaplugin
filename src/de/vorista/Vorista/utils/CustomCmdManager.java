package de.vorista.Vorista.utils;

import de.vorista.Vorista.VoristaPlugin;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Corvin on 02.10.2016.
 */
public class CustomCmdManager {

    private File file;
    private FileConfiguration config;
    private List<String> customCommands = new ArrayList<>();

    public CustomCmdManager() {
        file = new File(VoristaPlugin.getInstance().getDataFolder(), "customCommands.yml");

        if(!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        config = YamlConfiguration.loadConfiguration(file);

        loadCmds();
    }

    public List<String> getCustomCommands() { return customCommands; }
    public FileConfiguration getConfig() { return config; }

    /**
     * Adds a new command to the storage list and file
     * @param cmd name of the command to be added
     */
    public boolean createCommand(String cmd) {
        cmd = cmd.toLowerCase();
        if(!config.contains(cmd)) {
            config.set(cmd, Arrays.asList("Default line"));
            customCommands.add(cmd);
            saveFile();
            return true;
        }
        return false;
    }

    /**
     * Completely removes a command from the storage list and file
     * @param cmd command to be removed
     */
    public boolean removeCommand(String cmd) {
        cmd = cmd.toLowerCase();
        if(config.contains(cmd)) {
            config.set(cmd, null);
            customCommands.remove(cmd);
            saveFile();
            return true;
        }
        return false;
    }

    /**
     * Adds a line to a command
     * @param cmd command to add a line to
     * @param newLines list of new lines to be added
     */
    public boolean addToCommand(String cmd, List<String> newLines) {
        cmd = cmd.toLowerCase();
        if(config.contains(cmd)) {
            List<String> commandLines = config.getStringList(cmd);
            commandLines.addAll(newLines);
            config.set(cmd, commandLines);
            saveFile();
            return true;
        }
        return false;
    }

    /**
     * Removes a line from a command
     * @param cmd command to remove a line from
     * @param line index of the line to remove
     */
    public boolean removeFromCommand(String cmd, int line) {
        cmd = cmd.toLowerCase();
        if(config.contains(cmd)) {
            List<String> commandLines = config.getStringList(cmd);
            commandLines.remove(line);
            config.set(cmd, commandLines);
            saveFile();
            return true;
        }
        return false;
    }

    /**
     * Gets all lines specified for a given command.
     * @param cmd Command to get the lines of
     * @return StringList of lines
     */
    public List<String> getLinesOf(String cmd) {
        cmd = cmd.toLowerCase();
        if(config.contains(cmd)) return config.getStringList(cmd);
        else return null;
    }

    /**
     * Run's a custom command for a given player
     * @param cmd command to be run
     * @param player player sending the command
     */
    public void run(String cmd, Player player) {
        cmd = cmd.toLowerCase();
        /* Format:
        testcommand:
          - ex:/me is trying the test-command!
          - &aThis is a testing command!
         */
        List<String> lines = config.getStringList(cmd);
        for(String line : lines) {
            // Replacing variables
            line = line.replaceAll("%player%", player.getName());

            if(line.startsWith("ex:")) {
                // Execute a command
                Bukkit.dispatchCommand(player, line.split("ex:")[1].replaceFirst("/", ""));
            } else {
                // Send normal chat message
                player.sendMessage(Utils.color(line));
            }
        }
    }

    /**
     * Reloads the config
     */
    public void reload() {
        config = YamlConfiguration.loadConfiguration(file);
        loadCmds();
    }

    /**
     * Loads commands from the file into the list
     */
    private void loadCmds() {
        customCommands.clear();
        ConfigurationSection cs = config.getConfigurationSection("");
        customCommands.addAll(cs.getKeys(false).stream().collect(Collectors.toList()));
    }

    /**
     * Saves the file
     */
    private void saveFile() {
        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
