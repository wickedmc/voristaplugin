package de.vorista.Vorista.utils;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Corvin on 08.10.2016.
 */
public class TagMenu implements Listener {

    public static void show(Player player) {
        List<Tag> tagsToShow = new ArrayList<>();
        Inventory inventory = getDefaultInventory(player);
        for(Tag tag : Tag.getTags().values()) if (player.hasPermission(tag.getPermission())) tagsToShow.add(tag);
        for(Tag tag : tagsToShow) inventory.addItem(tag.getItem(player));
        player.openInventory(inventory);
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        Inventory inventory = event.getInventory();

        if(inventory.getName().equalsIgnoreCase(player.getName()+"'s Tags")) {
            event.setCancelled(true);
            ItemStack clickedItem = event.getCurrentItem();

            if(clickedItem != null && clickedItem.getType() == Material.NAME_TAG) {
                for(Tag tag : Tag.getTags().values()) {
                    if(Utils.compareItems(tag.getItem(player), clickedItem, false)) {
                        Tag.setActiveTagOf(player, tag.getName());
                        player.closeInventory();
                        player.sendMessage(Utils.color("&7[&9V&7] Tag &r"+tag.getPrefix()+" &7ausgewhält!"));
                    }
                }
            }
            if(clickedItem != null && clickedItem.getType() == Material.BARRIER) {
                Tag.setActiveTagOf(player, "Spieler");
                player.closeInventory();
                player.sendMessage(Utils.color("&7[&9V&7] Tag entfernt"));
            }

        }
    }

    @EventHandler
    public void onDrag(InventoryDragEvent event) {
        Player player = (Player) event.getWhoClicked();
        Inventory inventory = event.getInventory();

        if(inventory.getName().equalsIgnoreCase(player.getName()+"'s Tags")) {
            event.setCancelled(true);
        }
    }

    private static Inventory getDefaultInventory(Player player) {
        Inventory inventory = Bukkit.getServer().createInventory(player, 45, player.getName()+"'s Tags");

        ItemStack placeHolder = new ItemStackBuilder(Material.STAINED_GLASS_PANE).withDurability(15).withName("").build();
        for(int i = 27; i < 45; i++) inventory.setItem(i, placeHolder);
        inventory.setItem(40, new ItemStackBuilder(Material.BARRIER).withName("&cTag löschen").build());


        return inventory;
    }

}
