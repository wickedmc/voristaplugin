package de.vorista.Vorista.utils;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by Corvin on 31.10.2016.
 */
public class TimeUtils {

    public static String milliesToString(long start, long end) {
        long current = start;
        long difference = end-current; //millisekunden
        int years=0,weeks=0,days=0,hours=0,mins=0,secs=0;

        while(difference > 1000) { difference -= 1000; secs++; }
        while(secs > 60) { secs -= 60; mins++; }
        while(mins > 60) { mins -= 60; hours++; }
        while(hours > 24) { hours -= 24; days++; }
        while(days > 7) { days -= 7; weeks++; }
        while(weeks > 52) { weeks -= 52; years++; }

        return ((years > 0) ? ((years > 1) ? years+" Jahre, ":years+" Jahr, "):"")
                + ((weeks > 0) ? ((weeks > 1) ? weeks+" Wochen, ":weeks+" Woche, "):"")
                + ((days > 0) ? ((days > 1) ? days+" Tage, ":days+" Tag, "):"")
                + ((hours > 0) ? ((hours > 1) ? hours+" Stunden, ":hours+" Stunde, "):"")
                + ((mins > 0) ? ((mins > 1) ? mins+" Minuten, ":mins+" Minute, "):"")
                + ((secs > 0) ? ((secs > 1) ? secs+" Sekunden":secs+" Sekunde"):"");
    }

    public static String milliesToString(long millies) {
        int years=0,weeks=0,days=0,hours=0,mins=0,secs=0;

        while(millies > 1000) { millies -= 1000; secs++; }
        while(secs > 60) { secs -= 60; mins++; }
        while(mins > 60) { mins -= 60; hours++; }
        while(hours > 24) { hours -= 24; days++; }
        while(days > 7) { days -= 7; weeks++; }
        while(weeks > 52) { weeks -= 52; years++; }

        return ((years > 0) ? ((years > 1) ? years+" Jahre, ":years+" Jahr, "):"")
                + ((weeks > 0) ? ((weeks > 1) ? weeks+" Wochen, ":weeks+" Woche, "):"")
                + ((days > 0) ? ((days > 1) ? days+" Tage, ":days+" Tag, "):"")
                + ((hours > 0) ? ((hours > 1) ? hours+" Stunden, ":hours+" Stunde, "):"")
                + ((mins > 0) ? ((mins > 1) ? mins+" Minuten, ":mins+" Minute, "):"")
                + ((secs > 0) ? ((secs > 1) ? secs+" Sekunden":secs+" Sekunde"):"");
    }

    public static String milliesToDate(long millies) {

        if(millies == -1) {
            return "für immer";
        }

        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(millies);

        int mYear = calendar.get(Calendar.YEAR);
        int mMonth = calendar.get(Calendar.MONTH);
        int mDay = calendar.get(Calendar.DAY_OF_MONTH);
        int mHour = calendar.get(Calendar.HOUR_OF_DAY);
        int mMin = calendar.get(Calendar.MINUTE);
        int mSec = calendar.get(Calendar.SECOND);

        return mDay + "." + (mMonth+1) + "." + mYear + ", " + mHour + ":" + mMin + ":" + mSec + " Uhr";
    }

    public static long stringToMillies(String s) {
        long seconds;
        char[] zeitInput = s.toCharArray();
        String zeitChache = "";
        int years=0,weeks=0,days=0,hours=0,mins=0,secs=0;
        for(char c : zeitInput) {
            if(isNumber(c)) {
                zeitChache += c;
                continue;
            } else {
                switch(c) {
                    case 'y': years = Integer.parseInt(zeitChache); zeitChache = ""; continue;
                    case 'w': weeks = Integer.parseInt(zeitChache); zeitChache = ""; continue;
                    case 'd': days = Integer.parseInt(zeitChache); zeitChache = ""; continue;
                    case 'h': hours = Integer.parseInt(zeitChache); zeitChache = ""; continue;
                    case 'm': mins = Integer.parseInt(zeitChache); zeitChache = ""; continue;
                    case 's': secs = Integer.parseInt(zeitChache); zeitChache = ""; continue;
                    default: return 0;
                }
            }
        }
        seconds = secs+(mins*60)+(hours*3600)+(days*24*3600)+(weeks*7*24*3600)+(years*52*7*24*3600);
        return seconds*1000;
    }

    public static boolean isNumber(char ch) {
        char[] numbers = {'0','1','2','3','4','5','6','7','8','9'};
        for(char c : numbers) {
            if(c == ch) {
                return true;
            }
        }
        return false;
    }

}
