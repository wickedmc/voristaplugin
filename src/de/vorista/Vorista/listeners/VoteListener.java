package de.vorista.Vorista.listeners;

import com.vexsoftware.votifier.model.Vote;
import com.vexsoftware.votifier.model.VotifierEvent;
import de.vorista.Vorista.VoristaPlugin;
import de.vorista.Vorista.utils.ItemStackBuilder;
import de.vorista.Vorista.utils.Utils;
import de.vorista.Vorista.votecrates.VoteCrate;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.io.File;
import java.io.IOException;

/**
 * Created by Corvin on 08.10.2016.
 */
public class VoteListener implements Listener {

    private static File voteFile;
    private static FileConfiguration config;

    static {
        voteFile = new File(VoristaPlugin.getInstance().getDataFolder(), "votes.yml");
        if (!voteFile.exists()) {
            try {
                voteFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        config = YamlConfiguration.loadConfiguration(voteFile);
    }

    @EventHandler
    public void onVote(VotifierEvent event) {
        Vote vote = event.getVote();
        String name;
        Player player = Bukkit.getPlayer(vote.getUsername());
        if (player == null) {
            OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(vote.getUsername());

            if (offlinePlayer == null) {
                return;
            }

            name = offlinePlayer.getName();
            VoristaPlugin.sendToConsole("Vote von OfflinePlayer " + name + " bekommen.");
            String uuid = offlinePlayer.getUniqueId().toString();

            if(!doesPlayerExist(uuid)) registerNewPlayer(uuid);

            updatePlayer(uuid, 0, 1);

        } else {
            name = player.getName();
            VoristaPlugin.sendToConsole("Vote von Player " + name + " bekommen.");
            vote(player, 1);
            updatePlayer(player.getUniqueId().toString(), 1, 0);
        }
        Bukkit.broadcastMessage(Utils.color("&7[&9V&7] &9" + name + " &7hat gevotet und 3x &9&lVOTE &f&lKEY &7erhalten! &f/vote"));
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        if(hasOfflineVotes(player)) {
            vote(player, getOfflineVotes(player));
            resetOfflineVotes(player);
        }
    }

    private void saveFile() {
        try {
            config.save(voteFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void vote(Player player, int amount) {
        if(amount == 0) return;
        player.getInventory().addItem(new ItemStackBuilder(VoteCrate.getKey()).withAmount(3 * amount).build());
        player.sendMessage(Utils.color("&aDanke für's Voten!"));
    }

    private void registerNewPlayer(String uuid) {
        if (!config.contains(uuid)) {
            config.set(uuid + ".totalVotes", 0);
            config.set(uuid + ".offlineVotes", 0);
            config.set(uuid + ".lastVote", 0);
            saveFile();
        }
    }

    private void updatePlayer(String uuid, int onlineVotes, int offlineVotes) {
        if (config.contains(uuid)) {
            config.set(uuid + ".totalVotes", config.getInt(uuid + ".totalVotes") + (onlineVotes + offlineVotes));
            config.set(uuid + ".offlineVotes", config.getInt(uuid + ".offlineVotes") + offlineVotes);
            config.set(uuid + ".lastVote", System.currentTimeMillis());
            saveFile();
        }
    }

    private int getOfflineVotes(Player player) {
        String uuid = player.getUniqueId().toString();
        if (config.contains(uuid)) {
            return config.getInt(uuid + ".offlineVotes");
        }
        return 0;
    }

    private void resetOfflineVotes(Player player) {
        String uuid = player.getUniqueId().toString();
        if (config.contains(uuid)) {
            config.set(uuid + ".offlineVotes", 0);
            saveFile();
        }
    }

    private boolean hasOfflineVotes(Player player) {
        String uuid = player.getUniqueId().toString();
        if (config.contains(uuid)) {
            return config.getInt(uuid + ".offlineVotes") != 0;
        }
        return false;
    }

    private boolean doesPlayerExist(String uuid) {
        return config.contains(uuid);
    }
}
