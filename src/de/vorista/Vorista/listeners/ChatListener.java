package de.vorista.Vorista.listeners;

import de.vorista.Vorista.VoristaPlugin;
import de.vorista.Vorista.utils.CustomCmdManager;
import de.vorista.Vorista.utils.Tag;
import de.vorista.Vorista.utils.Utils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

/**
 * Created by Corvin on 14.09.2016.
 */
public class ChatListener implements Listener {

    @EventHandler(priority = EventPriority.NORMAL)
    public void onChat(AsyncPlayerChatEvent event) {
        //event.setCancelled(true);
        Player player = event.getPlayer();
        String message = event.getMessage();

        Tag tag = Tag.getActiveTagOf(player);
        if(tag == null) tag = Tag.getTag("Spieler");

        event.setFormat(Utils.color(tag.getPrefix()+" &8┃ &"+tag.getColor()+player.getName()+"&8: &f")+(player.hasPermission("vorista.premium") ? Utils.color(message):message));

    }

    @EventHandler(priority= EventPriority.HIGHEST)
    public void onPreCommand(PlayerCommandPreprocessEvent event) {
        String message = event.getMessage().replaceFirst("/", "");
        String command = message.split(" ")[0].replaceFirst("/", "");
        Player player = event.getPlayer();
        CustomCmdManager customCmd = VoristaPlugin.getCustomCommand();
        for(String cmd : customCmd.getCustomCommands()) {
            if(command.equalsIgnoreCase(cmd) && player.hasPermission("vorista.customcmd."+cmd)){
                customCmd.run(cmd, player);
                event.setCancelled(true);
            }
        }
    }

}
