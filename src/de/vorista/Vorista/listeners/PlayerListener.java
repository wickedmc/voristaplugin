package de.vorista.Vorista.listeners;

import de.vorista.Vorista.utils.Utils;
import de.vorista.Vorista.votecrates.VoteCrate;
import de.vorista.Vorista.votecrates.VoteReward;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Corvin on 07.10.2016.
 */
public class PlayerListener implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        ItemStack itemInHand = player.getItemInHand();

        if(event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            Block block = event.getClickedBlock();

            if(VoteCrate.isCrate(block)) {
                event.setCancelled(true);

                if(VoteCrate.isKey(itemInHand)) {

                    VoteReward reward = VoteCrate.getRandomReward();
                    if(reward.isCommand()) {
                        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), reward.getCommand().replaceAll("%player%", player.getName()));
                    } else {
                        if(!(player.getInventory().getContents().length >= player.getInventory().getSize())) {
                            player.getInventory().addItem(reward.getItem());
                        } else {
                            player.getWorld().dropItemNaturally(player.getLocation(), reward.getItem());
                        }
                    }
                    Bukkit.broadcastMessage(Utils.color("&9&lVOTE &f"+player.getName()+" &7hat &r"+reward.getName()+"&7 gewonnen! &f/vote"));
                    player.playSound(block.getLocation(), Sound.BLOCK_LAVA_POP, 0.5f, 9.5f);

                    Location loc = block.getLocation();
                    for(double y = loc.getY(); y < block.getY()+1; y+=0.1) {
                        double x = loc.getX() + 0.5 + (Math.sin(20 * y));
                        double z = loc.getZ() + 0.5 + (Math.cos(20 * y));
                        Location loc2 = new Location(loc.getWorld(), x, y, z);
                        block.getWorld().playEffect(loc2, Effect.LAVADRIP, 1);
                        block.getWorld().playEffect(loc2, Effect.LAVA_POP, 1);
                        block.getWorld().playEffect(loc2, Effect.FLAME, 1);
                    }

                    itemInHand.setAmount(itemInHand.getAmount()-1);
                    player.setItemInHand(itemInHand);
                    player.updateInventory();

                } else {
                    player.sendMessage(Utils.color("&7[&9V&7] Du brauchst einen Schlüssel um die Votecrate zu benuten! &f/vote"));
                    player.playSound(block.getLocation(), Sound.BLOCK_ANVIL_BREAK, 0.5f, 0.5f);
                }
            }
        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();

        if(player.hasPermission("vorista.premium") && player.getFirstPlayed()==0) {
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "money give "+player.getName()+" 500");
        }
    }

}
