package de.vorista.Vorista.listeners;

import de.vorista.Vorista.votecrates.VoteCrate;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Corvin on 21.10.2016.
 */
public class EntityKillListener implements Listener {

    private List<EntityType> mobs = Arrays.asList(EntityType.SPIDER, EntityType.ZOMBIE, EntityType.BLAZE, EntityType.CAVE_SPIDER, EntityType.CREEPER,
            EntityType.ENDERMAN, EntityType.GHAST, EntityType.GUARDIAN, EntityType.MAGMA_CUBE, EntityType.PIG_ZOMBIE, EntityType.SHULKER, EntityType.SKELETON,
            EntityType.SLIME);

    @EventHandler
    public void onEntityKill(EntityDeathEvent event) {
        Entity entity = event.getEntity();

        if(mobs.contains(entity.getType())) {
            Entity killer = ((LivingEntity)entity).getKiller();
            if(killer != null && killer.hasPermission("vorista.premium")) {
                // Drop at defined chance
                double d = Math.random();
                double chance = 2;

                if(killer.hasPermission("vorista.vorbesteller")) chance = 3;
                ItemStack key = VoteCrate.getKey();
                key.setAmount(1);
                if(d < (chance/100)) killer.getWorld().dropItemNaturally(event.getEntity().getLocation(), key);
            }
        }
    }

}
