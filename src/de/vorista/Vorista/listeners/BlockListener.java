package de.vorista.Vorista.listeners;

import de.vorista.Vorista.utils.Utils;
import de.vorista.Vorista.votecrates.VoteCrate;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Corvin on 01.10.2016.
 */
public class BlockListener implements Listener {

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        Player player = event.getPlayer();
        ItemStack itemInHand = player.getItemInHand();
        // Place Votecrate
        if(VoteCrate.isCrateItem(itemInHand) && player.hasPermission("vorista.votecrate.place")) {
            VoteCrate.addCrate(event.getBlockPlaced(), true);
            player.sendMessage(Utils.color("&7[&9V&7] Votecrate gesetzt!"));
        }
        if(VoteCrate.isKey(itemInHand)) {
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        Player player = event.getPlayer();
        ItemStack itemInHand = player.getItemInHand();
        // Destroy Votecrate
        if(VoteCrate.isCrate(event.getBlock())) {
            event.setCancelled(true);
            if(player.hasPermission("vorista.votecrate.break") && itemInHand.getType() == Material.GOLD_AXE) {
                event.setCancelled(false);
                VoteCrate.removeCrate(event.getBlock());
                player.sendMessage(Utils.color("&7[&9V&7] Votecrate entfernt!"));
            }
        }
    }

}
